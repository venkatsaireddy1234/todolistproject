/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { Navbar, Container } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import { Link } from "react-router-dom";
// import Sidebar from "./Sidebar";
import { Switch } from "react-router-dom";
import { Route } from "react-router-dom";
import ProjectDisplay from "./ProjectDisplay";
import Accordian from "./Accordian";


class Header extends Component {
  render() {
    return (
      <React.Fragment>
        <Navbar variant="dark" bg="danger">
          <div className="d-flex justify-content-evenly">
            <div class="hamburger-menu">
              <input id="menu__toggle" type="checkbox" />
              <label class="menu__btn" for="menu__toggle">
                <span></span>
              </label>
              <ul class="menu__box">
                <div className="d-flex">
                  <Accordian />
                </div>
              </ul>
            </div>
            <Link to="/">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="27"
                height="27"
                fill="currentColor"
                class="bi bi-house-door"
                viewBox="0 0 16 16"
                className="text-light"
              >
                <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z" />
              </svg>
            </Link>
          </div>
          <Container style={{ height: `${30}px` }}>
            <Navbar.Brand></Navbar.Brand>
          </Container>
        </Navbar>
        <div>
          <Switch>
            <Route
              exact
              path="/projects/:venkat"
              component={(props) => <ProjectDisplay {...props} />}
            />
          </Switch>
        </div>
      </React.Fragment>
    );
  }
}

export default Header;
