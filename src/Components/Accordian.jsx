import React, { Component } from "react";
import Accordion from "react-bootstrap/Accordion";
import axios from "axios";
import { Link } from "react-router-dom";
import "../App.css";
import Popup from "./Popup";
import { v4 as uuid } from "uuid";
// import Deleteproject from "./Deleteproject";
import { DropdownButton, Dropdown } from "react-bootstrap";
import { Modal } from "react-bootstrap";
import { Button } from "react-bootstrap";
// import DropdownItem from "react-bootstrap/esm/DropdownItem";
import { Form } from "react-bootstrap";

class Accordian extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: false,
      show: false,
      showd: false,
      newprojectInput: "",
      showEditProject: false,
      editProjectInput: "",
      favorite: false,
    };
  }

  componentDidMount() {
    let options = {
      method: "GET",
      headers: {
        Authorization: "Bearer f0dcdf6c28e277d0648aeb38f1e7f1aa2233838d",
      },
    };
    axios
      .get("https://api.todoist.com/rest/v1/projects", options)
      .then((response) =>
        this.setState({ data: response.data, loading: true })
      );
  }

  handledelete = () => {
    this.setState({
      showd: true,
    });
  };

  handledeleteClose = () => {
    this.setState({
      showd: false,
    });
  };

  handleShow = () => {
    this.setState({
      show: true,
    });
  };

  handleClose = () => {
    this.setState({
      show: false,
    });
  };

  handleChange = (e) => {
    this.setState({
      newprojectInput: e.target.value,
    });
  };
  handledeleteProject = (project) => {
    // e.preventDefault();
    axios
      .delete(`https://api.todoist.com/rest/v1/projects/${project.id}`, {
        headers: {
          "X-Request-Id": uuid(),
          Authorization: "Bearer f0dcdf6c28e277d0648aeb38f1e7f1aa2233838d",
        },
      })
      .then(() => {
        const remianingprojects = this.state.data.filter(
          (each) => each.id !== project.id
        );
        console.log(remianingprojects);
        this.setState({
          data: [...remianingprojects],
          showd: false,
        });
      });
  };
  handleadd = (e) => {
    console.log(e);
    e.preventDefault();
    axios
      .post(
        "https://api.todoist.com/rest/v1/projects",
        { name: this.state.newprojectInput, favorite: this.state.favorite },
        {
          headers: {
            "Content-Type": "application/json",
            "X-Request-Id": uuid(),
            Authorization: "Bearer f0dcdf6c28e277d0648aeb38f1e7f1aa2233838d",
          },
        }
      )
      .then((response) => response.data)
      .then((newproject) => {
        this.setState({
          data: [...this.state.data, newproject],
          show: false,
          newprojectInput: "",
        });
      });
  };
  handleEdit = () => {
    this.setState({
      showEditProject: !this.state.showEditProject,
    });
  };
  handleEditClose = () => {
    this.setState({
      showEditProject: false,
    });
  };
  handleEditdeleteClose = () => {
    this.setState({
      showEditProject: this.state.showEditProject,
    });
  };

  handleSave = (id) => {
    // console.log(this.state.editProjectInput);
    // console.log(id);
    axios
      .post(
        `https://api.todoist.com/rest/v1/projects/${id}`,
        { name: this.state.editProjectInput },
        {
          headers: {
            "Content-Type": "application/json",
            "X-Request-Id": uuid(),
            Authorization: "Bearer f0dcdf6c28e277d0648aeb38f1e7f1aa2233838d",
          },
        }
      )
      .then(() => {
        const updatedInput = this.state.data.map((each) => {
          if (each.id === id) {
            each.name = this.state.editProjectInput;
            return each;
          } else {
            return each;
          }
        });
        this.setState({
          data: updatedInput,
          showEditProject: false,
        });
      });
  };
  handleEditName = (e) => {
    this.setState({
      editProjectInput: e.target.value,
    });
  };

  handleFavorite = () => {
    this.setState({
      favorite: !this.state.favorite,
    });
    console.log(this.state.favorite);
  };

  filterFavoriteProjects = () => {
    let filteredFvaoriteProjects = this.state.data.filter(
      (each) => each.favorite === true
    );
    if (filteredFvaoriteProjects) {
      return filteredFvaoriteProjects;
    } else {
      return [];
    }
  };
  handleRemoveFavorites = (each) => {
    console.log(each.id);
    const article = { favorite: false, name: `${each.name}` };
    const headers = {
      Authorization: "Bearer f0dcdf6c28e277d0648aeb38f1e7f1aa2233838d",
      "Content-Type": "application/json",
      "X-Request-Id": uuid(),
    };
    axios.post(`https://api.todoist.com/rest/v1/projects/${each.id}`, article, {
      headers,
    });
    this.setState({
      data: this.state.data.map((project) => {
        if (project.id === each.id) {
          return {
            ...project,
            favorite: !project.favorite,
          };
        }
        return project;
      }),
    });
  };
  handleAddToFavorites = (each) => {
    console.log(each.id);
    const article = { favorite: true };
    const headers = {
      Authorization: "Bearer f0dcdf6c28e277d0648aeb38f1e7f1aa2233838d",
      "Content-Type": "application/json",
      "X-Request-Id": uuid(),
    };
    axios.post(`https://api.todoist.com/rest/v1/projects/${each.id}`, article, {
      headers,
    });

    this.setState({
      data: this.state.data.map((project) => {
        if (project.id === each.id) {
          return {
            ...project,
            favorite: !project.favorite,
          };
        }
        return project;
      }),
    });
    console.log(!this.state.favorite);
  };

  render() {
    const filterFavoriteProjects = this.filterFavoriteProjects();
    return (
      <div className="sidebar">
        <div>
          <Accordion>
            <Accordion.Item
              className="acc1"
              style={{ backgroundColor: "#e9ecef", width: `${30}vh` }}
            >
              <Accordion.Header>Favorites</Accordion.Header>
              <Accordion.Body>
                {this.state.loading &&
                  filterFavoriteProjects.map((each, index) => {
                    return (
                      <>
                        <Link
                          className="display"
                          to={{
                            pathname: `/projects/${each.id}`,
                            item: { data: { each } },
                          }}
                        >
                          <div key={index}>{each.name}</div>
                        </Link>
                        <DropdownButton
                          as="ButtonGroup"
                          key="variant"
                          id="dropdown-variants-Secondary"
                          variant="variant.toLowerCase()"
                          title=""
                          className="del1"
                        >
                          <Dropdown.Item onClick={this.handleEdit}>
                            Edit Project{" "}
                          </Dropdown.Item>
                          <Modal
                            show={this.state.showEditProject}
                            onHide={this.handleEditdeleteClose}
                            animation={false}
                          >
                            <Modal.Header>
                              <Modal.Title> Edit Project</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <Form>
                                <Form.Group
                                  className="mb-3"
                                  controlId="exampleForm.ControlInput1"
                                >
                                  <Form.Label>Name</Form.Label>
                                  <Form.Control
                                    // value={this.state.editProjectInput}
                                    onChange={this.handleEditName}
                                    type="text"
                                    autoFocus
                                  />
                                </Form.Group>
                                <Form.Group
                                  className="mb-3"
                                  controlId="exampleForm.ControlTextarea1"
                                ></Form.Group>
                              </Form>
                            </Modal.Body>
                            <Modal.Footer>
                              <Button
                                variant="light"
                                show={this.state.showEditProject}
                                onClick={this.handleEditClose}
                              >
                                Cancel
                              </Button>
                              <Button
                                variant="danger"
                                show={!this.state.showEditProject}
                                onClick={() => {
                                  this.handleSave(each.id);
                                }}
                              >
                                save
                              </Button>
                            </Modal.Footer>
                          </Modal>

                          <Dropdown.Item
                            onClick={() => this.handleRemoveFavorites(each)}
                          >
                            Remove from Favorites{" "}
                          </Dropdown.Item>
                        </DropdownButton>
                      </>
                    );
                  })}
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        </div>
        <div>
          <Popup
            onhandleadd={this.handleadd}
            onhandleClose={this.handleClose}
            onhandleChange={this.handleChange}
            onhandleShow={this.handleShow}
            show={this.state.show}
            newprojectInput={this.state.newprojectInput}
            onhandleFavorite={this.handleFavorite}
          />
        </div>

        <div className="d-flex">
          <div>
            <Accordion>
              <Accordion.Item
                className="acc2"
                style={{ backgroundColor: "#e9ecef", width: `${30}vh` }}
              >
                <Accordion.Header> Projects </Accordion.Header>
                <Accordion.Body
                  style={{ width: `${34.4}vh`, marginLeft: `${-17}px` }}
                >
                  {this.state.loading
                    ? this.state.data.map((each, index) => {
                        return (
                          <div className="d-flex justify-content-between">
                            <Link
                              to={{
                                pathname: `/Projects/${each.id}`,
                                item: { data: { each } },
                              }}
                            >
                              <div className="text-dark" key={index}>
                                {each.name}
                              </div>
                            </Link>

                            <DropdownButton
                              as="ButtonGroup"
                              key="variant"
                              id="dropdown-variants-Secondary"
                              variant="variant.toLowerCase()"
                              title=""
                            >
                              <Dropdown.Item> Add project Above</Dropdown.Item>
                              <Dropdown.Item onClick={this.handleEdit}>
                                Edit Project{" "}
                              </Dropdown.Item>
                              <Modal
                                show={this.state.showEditProject}
                                onHide={this.handleEditdeleteClose}
                                animation={false}
                              >
                                <Modal.Header>
                                  <Modal.Title> Edit Project</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                  <Form>
                                    <Form.Group
                                      className="mb-3"
                                      controlId="exampleForm.ControlInput1"
                                    >
                                      <Form.Label>Name</Form.Label>
                                      <Form.Control
                                        // value={this.state.editProjectInput}
                                        onChange={this.handleEditName}
                                        type="text"
                                        autoFocus
                                      />
                                    </Form.Group>
                                    <Form.Group
                                      className="mb-3"
                                      controlId="exampleForm.ControlTextarea1"
                                    ></Form.Group>
                                  </Form>
                                </Modal.Body>
                                <Modal.Footer>
                                  <Button
                                    variant="light"
                                    show={this.state.showEditProject}
                                    onClick={this.handleEditClose}
                                  >
                                    Cancel
                                  </Button>
                                  <Button
                                    variant="danger"
                                    show={!this.state.showEditProject}
                                    onClick={() => {
                                      this.handleSave(each.id);
                                    }}
                                  >
                                    save
                                  </Button>
                                </Modal.Footer>
                              </Modal>

                              <Dropdown.Item> share project</Dropdown.Item>
                              <Dropdown.Item
                                onClick={() => this.handleAddToFavorites(each)}
                              >
                                {" "}
                                Add to favorites
                              </Dropdown.Item>
                              <Dropdown.Item> Duplicate project</Dropdown.Item>
                              <Dropdown.Item>
                                {" "}
                                Email tasks to this project
                              </Dropdown.Item>
                              <Dropdown.Item>
                                {" "}
                                Project calendar feed
                              </Dropdown.Item>
                              <Dropdown.Item> Archive project</Dropdown.Item>
                              <Dropdown.Item onClick={this.handledelete}>
                                Delete Project
                              </Dropdown.Item>

                              <Modal
                                show={this.state.showd}
                                onHide={this.handledeleteClose}
                                animation={false}
                              >
                                <Modal.Header closeButton>
                                  <Modal.Title>Caution!</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                  Are You sure you want to delete this project?
                                </Modal.Body>
                                <Modal.Footer>
                                  <Button
                                    variant="light"
                                    onClick={this.handledeleteClose}
                                  >
                                    Cancel
                                  </Button>
                                  <Button
                                    variant="primary"
                                    className="bg-danger"
                                    onClick={() =>
                                      this.handledeleteProject(each)
                                    }
                                  >
                                    Delete
                                  </Button>
                                </Modal.Footer>
                              </Modal>
                            </DropdownButton>
                          </div>
                        );
                      })
                    : "loading..."}
                </Accordion.Body>
              </Accordion.Item>
            </Accordion>
          </div>
        </div>
      </div>
    );
  }
}

export default Accordian;
