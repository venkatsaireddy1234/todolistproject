import React, { Component } from 'react'
import {Modal} from 'react-bootstrap'
import { Button,Form } from 'react-bootstrap';


class Popup extends Component {
    render() { 
    // console.log(this.props)
    const {onhandleadd,onhandleShow,onhandleClose,onhandleChange,onhandleFavorite,}=this.props
    return (<>
      <Button style={{backgroundColor:"none", fontSize:`${25}px`}} variant=""  className="plus" onClick={onhandleShow}>+</Button>
        <Modal  show={this.props.show} onHide={onhandleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Add Project</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          
            <Form >
              <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                <Form.Label>Name</Form.Label>
               
                <Form.Control
                  value={this.props.newprojectInput} onChange={onhandleChange} type="text" 
                  autoFocus
                />
              </Form.Group>
              <label class="switch">
                <input type="checkbox"
                onChange={()=>onhandleFavorite()}
                /> Add to Favorites
                <span class="slider round"></span>
              </label>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlTextarea1"
              >
                {/* <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                        Dropdown Button
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                        <Dropdown.Item href="#/action-1">Red</Dropdown.Item>
                        <Dropdown.Item href="#/action-2">organge</Dropdown.Item>
                        <Dropdown.Item href="#/action-3">Green</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown> */}
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
          
            <Button variant="light" show={this.props.show} onClick={onhandleClose}>
              Cancel
            </Button>
            <Button  variant="danger" onClick={onhandleadd} >
              Add
            </Button>
          </Modal.Footer>
        </Modal>
      </>);
    }
}
 
export default Popup;