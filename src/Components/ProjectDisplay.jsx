import React, { Component } from "react";
import axios from "axios";
import Button from "react-bootstrap/Button";
import { v4 as uuid } from "uuid";
import { DropdownButton, Dropdown } from "react-bootstrap";
// import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
// import DropdownItem from "react-bootstrap/esm/DropdownItem";

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: false,
      add: false,
      show: true,
      newContent: "",
      inputValue: "",
      isChecked: false,
      hideOperation: false,
      startDate: new Date(),
      showEditTaskId:""
    };
    this.handleChange = this.handleChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }
  componentDidMount() {
    let options = {
      method: "GET",
      headers: {
        Authorization: "Bearer f0dcdf6c28e277d0648aeb38f1e7f1aa2233838d",
      },
    };
    axios
      .get("https://api.todoist.com/rest/v1/tasks", options)
      .then((response) =>
        this.setState({ data: response.data, loading: true })
      );
  }

  addtask = () => {
    this.setState({
      add: !this.state.add,
      show: !this.state.show,
    });
  };

  cancel = () => {
    this.setState({
      add: !this.state.add,
      show: !this.state.show,
    });
  };

  updateInputValue = (e) => {
    this.setState({
      newContent: e.target.value,
    });
  };

  addContent = (e) => {
    e.preventDefault();
    axios
      .post(
        "https://api.todoist.com/rest/v1/tasks",
        {
          content: this.state.newContent,
          project_id: this.props.location.item.data.each.id,
        },
        {
          headers: {
            "Content-Type": "application/json",
            "X-Request-Id": uuid(),
            Authorization: "Bearer f0dcdf6c28e277d0648aeb38f1e7f1aa2233838d",
          },
        }
      )
      .then((response) => response.data)
      .then((newContent) => {
        this.setState({
          data: [...this.state.data, newContent],
          add: !this.state.add,
          show: !this.state.show,
          newContent: "",
        });
      });
  };

  deletetask = (e, id) => {
    e.preventDefault();
    console.log(id);
    axios
      .delete(`https://api.todoist.com/rest/v1/tasks/${id}`, {
        headers: {
          Authorization: "Bearer f0dcdf6c28e277d0648aeb38f1e7f1aa2233838d",
        },
      })
      .then(() => {
        const remianingtasks = this.state.data.filter((each) => each.id !== id);
        console.log(remianingtasks);
        this.setState({
          data: [...remianingtasks],
        });
      });
  };

  handleCheckbox = (id) => {
    const headers = {
      Authorization: "Bearer f0dcdf6c28e277d0648aeb38f1e7f1aa2233838d",
    };
    let task = this.state.data.find((task) => task.id === id);
    let url = "";
    if (task.completed) {
      url = `https://api.todoist.com/rest/v1/tasks/${id}/reopen`;
    } else {
      url = `https://api.todoist.com/rest/v1/tasks/${id}/close`;
    }
    axios
      .post(url, null, {
        headers,
      })
      .then(() => {
        this.setState({
          data: this.state.data.map((task) => {
            if (task.id === id) {
              return {
                ...task,
                completed: !task.completed,
              };
            }
            return task;
          }),
        });
      });
  };

  handleActivetask = () => {
    let filteredTask = this.state.data.filter(
      (each) =>
        each.project_id === parseInt(this.props.match.params.venkat) &&
        each.completed === false
    );
    if (filteredTask) {
      return filteredTask;
    } else {
      return [];
    }
  };

  handleCompletedtask = (e) => {
    let filteredTask = this.state.data.filter(
      (each) =>
        each.project_id === parseInt(this.props.match.params.venkat) &&
        each.completed !== false
    );
    if (filteredTask) {
      return filteredTask;
    } else {
      return [];
    }
  };

  hidingTasks = () => {
    this.setState({
      hideOperation: !this.state.hideOperation,
    });
  };
  handleChange(date) {
    this.setState({
      startDate: date,
    });
  }
  onFormSubmit(e) {
    e.preventDefault();
    console.log(this.state.startDate);
  }

  handleShowEditTask = (task) => {
    console.log("before ", this.state.showEditTaskId);
    this.setState(
      {
        showEditTaskId: task.id,
      },
      () => {
        console.log("after ", this.state.showEditTaskId);
      }
    );
  };
  render() {
    // const { data } = this.state;
    const displayActiveTasks = this.handleActivetask();
    const displayCompletedTask = this.handleCompletedtask();
    const hideBtn = this.state.hideOperation
      ? "Hide completed tasks"
      : "Show completed tasks";
    // displayActiveTasks = data.filter((item) => {
    //   if (item.project_id === parseInt(this.props.match.params.venkat)) {
    //     return item;
    //   } else {
    //     return null;
    //   }
    // });
    return (
      <>
        <div className="main">
          <h4>{this.props.location.item.data.each.name}</h4>
          <hr />
          <div className="namedel">
            <DropdownButton
              as="ButtonGroup"
              key="variant"
              id="dropdown-variants-Secondary"
              variant="variant.toLowerCase()"
              title=""
            >
              <Dropdown.Item>Edit project</Dropdown.Item>
              <Dropdown.Item>Add Section</Dropdown.Item>
              <Dropdown.Item>Import a template</Dropdown.Item>
              <Dropdown.Item>Export a template</Dropdown.Item>
              <Dropdown.Item>Duplicate a project</Dropdown.Item>
              <Dropdown.Item>Email tasks to a project</Dropdown.Item>
              <Dropdown.Item>Project calendar feed</Dropdown.Item>
              <Dropdown.Item
                isCompleted={this.state.isCompleted}
                onClick={this.hidingTasks}
              >
                {hideBtn}
              </Dropdown.Item>
              <Dropdown.Item>Archive </Dropdown.Item>
              <Dropdown.Item>Delete project </Dropdown.Item>
            </DropdownButton>
          </div>
          <div>
            <div className="d-flex flex-column">
            {}
              {displayActiveTasks.map((each) =>              
               (
                <div key={each.id}>
                  <div>
                    <input
                      onChange={() => this.handleCheckbox(each.id)}
                      value="this.state.inputValue"
                      className="radio"
                      id="checkbox"
                      checked={each.completed}
                      type="checkbox"
                    />
                    <label
                      className={
                        each.completed ? "check-box strikeOff" : "check-box"
                      }
                      htmlFor="checkbox"
                    >
                      {each.content}
                    </label>
                  </div>
                  <div className="del">
                    <DropdownButton
                      as="ButtonGroup"
                      key="variant"
                      id="dropdown-variants-Secondary"
                      variant="variant.toLowerCase()"
                      title=""
                    >
                      <Dropdown.Item
                        onClick={() => this.handleShowEditTask(each)}
                      >
                        Edit Task
                      </Dropdown.Item>
                      <Dropdown.Item
                        onClick={(e) => this.deletetask(e, each.id)}
                      >
                        Delete Task
                      </Dropdown.Item>
                    </DropdownButton>
                  </div>
                  <hr />
                </div>
              ))}
            </div>
          </div>
          {this.state.show ? (
            <Button
              onClick={this.addtask}
              className="text-danger"
              variant="None"
            >
              {" "}
              + Add task{" "}
            </Button>
          ) : null}
          <div className="d-flex">
            <div>
              {this.state.add ? (
                <div className="d-flex flex-column">
                  <div>
                    <div>
                      <textarea
                        add={this.state.add}
                        value={this.state.newContent}
                        onChange={(event) => this.updateInputValue(event)}
                        type="text"
                        placeholder="Task Name"
                        style={{
                          width: `${60}vh`,
                          height: `${15}vh`,
                          borderRadius: `${10}px`,
                          borderInlineColor: "GrayText",
                        }}
                      />
                      {/* <form onSubmit={this.onFormSubmit}> */}
                        {/* <div className="form-group"> */}
                          {/* <DatePicker
                            // selected={this.state.startDate}
                            onChange={this.handleChange}
                            name="startDate"
                            dateFormat="MM/dd/yyyy"
                          /> */}
                          {/* <button className="btn btn-secondary">Due Data</button> */}
                        {/* </div> */}
                      {/* </form> */}
                    </div>
                    <div></div>
                  </div>
                  <div>
                    <Button
                      onClick={this.addContent}
                      variant="danger"
                      className="m-1"
                    >
                      Add Task
                    </Button>{" "}
                    <Button
                      onClick={this.cancel}
                      variant="secondary"
                      className="m-1"
                    >
                      Cancel
                    </Button>{" "}
                  </div>
                </div>
              ) : null}
            </div>
          </div>
          <div>
            {this.state.hideOperation ? (
              <div className="d-flex flex-column">
                {displayCompletedTask.map((each) => {
                  return (
                    <div>
                      <div>
                        <input
                          onChange={() => this.handleCheckbox(each.id)}
                          value="this.state.inputValue"
                          className="checkbox-circle"
                          id="checkbox"
                          checked={each.completed}
                          type="checkBox"
                        />
                        <label
                          className={
                            each.completed ? "check-box strikeOff" : "check-box"
                          }
                          htmlFor="checkbox"
                        >
                          {each.content}
                        </label>
                      </div>
                      <div className="del">
                        <DropdownButton
                          as="ButtonGroup"
                          key="variant"
                          id="dropdown-variants-Secondary"
                          variant="variant.toLowerCase()"
                          title=""
                        >
                          <Dropdown.Item
                            onClick={(e) => this.deletetask(e, each.id)}
                          >
                            Delete Task
                          </Dropdown.Item>
                          <Dropdown.Item>Edit Task</Dropdown.Item>
                        </DropdownButton>
                      </div>
                      <hr />
                    </div>
                  );
                })}
              </div>
            ) : null}
          </div>
        </div>
      </>
    );
  }
}

export default Main;
